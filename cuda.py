import torch
from torch import nn

print("Generating tensors")
X = torch.arange(-3, 3, 0.1).view(-1,1)
f = 2 * X - .3
Y = f + 0.1*torch.randn(X.size())

print("Sending to GPU")
device = torch.device('cuda:0')
X = X.to(device)
Y = Y.to(device)

class linefitmodel(nn.Module):
  def __init__(self):
    super().__init__()
    self.fc1 = nn.Linear(1, 1)

  def forward(self, x):
    x = self.fc1(x)

    return x

print("Creating model")
model = linefitmodel()
print("Sending to GPU")
model = model.to(device)

print("Creating crit and opt")
criterion = nn.MSELoss()
optimizer = torch.optim.SGD(model.parameters(), lr = 0.01)

for epoch in range(400):
    # Forward pass: Compute predicted y by passing
    # x to the model
    pred_y = model(X)

    # Compute and print loss
    loss = criterion(pred_y, Y)

    # Zero gradients, perform a backward pass,
    # and update the weights.
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
    print('epoch {}, loss {}'.format(epoch, loss.item()))

print("Sending model back to CPU")
model = model.to('cpu')
print(f"Our model predicts {model(torch.tensor([6.]))} for 2 * 6 - 0.3")
